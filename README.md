# How to use
## 0. APP build and Docker build
If you need to rebuild the microservice and/or build the docker image with the changer
```bash
GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" -o apiexercise
docker build -t <repository>/api-exercise:latest .
docker push <repository>/api-exercise:latest
```
If you are using the [Kubernetes deployment](https://gitlab.com/nabil.tahri/API-Exercise#kubernetes-recommended-) don't forget to change de ```spec.spec.containers[0].image``` value
## 1. Starting mongodb
### 1.1. Docker-compose
If you need to change the database name or the username and the pwd you can make the modifications ```./dev/mongo-init.js```
To run a configured mongodb using docker-compose :
```bash
chmod +x ./dev/mongo.sh
./dev/mongo.sh
```

### 1.2. Kubernetes (recommended)
After login to you desired Kubernetes cluster you need to create three Kubernetes Objects by executing this cmd:
NB: You need to create a new namespace for the mongo deployment, it isn't a good practice to deploy into default namespace
Optional: ```kubectl create -f ./kubernetes/ns.yml ``` 
```bash
#Creating the PVC
kubectl create -f ./kubernetes/PersistentVolumeClaim.yml 

#Creating the Deployment
kubectl create -f ./kubernetes/mongo-deployment.yml

#Creating the service
kubectl create -f ./kubernetes/mongo-service.yml 

```
Exposing the service - Not recommended but for simplicity I choose this way. You are going to edit the service object and modify the Type:
````
kubectl edit svc mongodb
````
Change ```Type: ClusterIP``` to ```Type: LoadBalancer```
That way the Kubernetes cluster will affect a public IP to your mongodb service
## 2. Microservice
### 2.1. Docker-compose
Before executing the api shell script you need to check the docker-compose VAR ENV if there is any changes to do ... 
```bash
chmod +x ./api.sh
./api.sh
```
### 2.2. Kubernetes (recommended) `*`
```bash
#Creating the Deployment
kubectl create -f ./kubernetes/deployment.yml

#Creating the service
kubectl create -f ./kubernetes/service.yml
```
## 3. Using the gitlab deployment. (Automatic way)
Change the code if you need then execute:
````bash
GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" -o apiexercise
git commit -m "changes propose" && git push
````
You can check the [gitlab pipeline](https://gitlab.com/nabil.tahri/API-Exercise/pipelines) for the deployment status
The microservice will be reachable at this URL:  http://34.67.86.136:8080/people 

# API-exercise

This exercise is to assess your technical proficiency with Software Engineering, DevOps and Infrastructure tasks.
There is no need to do all the exercises, but try to get as much done as you can, so we can get a good feel of your skillset.  Don't be afraid to step outside your comfort-zone and try something new.

If you have any questions, feel free to reach out to us.

## Exercise

This exercise is split in several subtasks. We are curious to see where you feel most comfortable and where you struggle.

### 0. Fork this repository
All your changes should be made in a **private** fork of this repository. When you're done please, please:
* Share your fork with the **container-solutions-test** user (Settings -> Members -> Share with Member)
* Make sure that you grant this user the Reporter role, so that our reviewers can check out the code using Git.
* Reply to the email that asked you to do this API exercise, with a link to the repository that the **container-solutions-test** user now should have access to.

### 1. Setup & fill database
In the root of this project you'll find a csv-file with passenger data from the Titanic. Create a database and fill it with the given data. SQL or NoSQL is your choice.

### 2. Create an API
Create an HTTP-API (e.g. REST) that allows reading & writing (maybe even updating & deleting) data from your database.
Tech stack and language are your choice. The API we would like you to implement is described in [API.md](./API.md)

### 3. Dockerize
Automate setup of your database & API with Docker, so it can be run everywhere comfortably with one or two commands.
The following build tools are acceptable:
 * docker
 * docker-compose
 * groovy
 * minikube (see 4.)

No elaborate makefiles please.

#### Hints

- [Docker Install](https://www.docker.com/get-started)

### 4. Deploy to Kubernetes
Enable your Docker containers to be deployed on a Kubernetes cluster.

#### Hints

- Don't have a Kubernetes cluster to test against?
  - [MiniKube](https://kubernetes.io/docs/setup/minikube/) (free, local)
  - [GKE](https://cloud.google.com/kubernetes-engine/) (more realistic deployment, may cost something)

### 5. Whatever you can think of
Do you have more ideas to optimize your workflow or automate deployment? Feel free to go wild and dazzle us with your solutions.
