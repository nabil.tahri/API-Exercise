package main

import (
	"fmt"
	. "github.com/API-Exercise/Models"
	"github.com/ant0ine/go-json-rest/rest"
	"log"
	"net/http"
	"os"
	"strconv"
)
func getEnvironment() (*Environment, error) {
	DATABASENAME := os.Getenv("DATABASENAME")
	COLLECTION_NAME := os.Getenv("COLLECTION_NAME")
	MONGO_URI := os.Getenv("MONGO_URI")
	MONGO_USERNAME := os.Getenv("MONGO_USERNAME")
	MONGO_PASSWORD := os.Getenv("MONGO_PASSWORD")
	LOCAL_DATABASE, err := strconv.ParseBool(os.Getenv("LOCAL_DATABASE"))

	return &Environment{
		DATABASENAME:          DATABASENAME,
		COLLECTION_NAME:       COLLECTION_NAME,
		MONGO_URI:             MONGO_URI,
		MONGO_USERNAME:        MONGO_USERNAME,
		MONGO_PASSWORD:        MONGO_PASSWORD,
		LOCAL_DATABASE:		   LOCAL_DATABASE,
	}, err
}

func main() {
	env, err := getEnvironment()
	if err != nil{
		fmt.Errorf("Error :%+v\n", err)
	}
	service := MakeMicroService(env)
	api := rest.NewApi()
	api.Use(rest.DefaultDevStack...)
	router, err := rest.MakeRouter(
		//rest.Get("/fillup", service.FillUp),
		rest.Get("/health", service.Health),
		rest.Get("/people", service.PeopleList),
		rest.Post("/people", service.PeopleAdd),
		rest.Get("/people/:uuid", service.PersonGet),
		rest.Put("/people/:uuid", service.PersonUpdate),
		rest.Delete("/people/:uuid", service.PersonDelete),
	)
	if err != nil {
		log.Fatal(err)
	}
	api.SetApp(router)
	log.Fatal(http.ListenAndServe(":8080", api.MakeHandler()))
}




