db.createUser(
        {
            user: "admin",
            pwd: "admin",
            roles: [
                {
                    role: "readWrite",
                    db: "exercise"
                },
                {
                    role: "dbAdmin",
                    db: "exercise"
                }
            ]
        }
);