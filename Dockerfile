


############################
# STEP 1 build executable binary
############################
#
#FROM golang:latest AS builder
#ENV GO111MODULE=on
#
##RUN curl https://raw.githubusercontent.com/golang/dep/master/install.sh | sh
#
#WORKDIR $GOPATH/src/api/exercice
#COPY . .
#RUN pwd && ls -la
#RUN go mod tidy
#
#RUN go get -d
#
#RUN ls -la $GOPATH/src/api/exercice
#
#RUN GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" -o /go/bin/apiexercise
#RUN ls -la /go/bin/
############################
# STEP 2 build a small image
############################
FROM alpine

EXPOSE 8080
#COPY --from=builder /go/bin/apiexercise /usr/local/bin/apiexercise
RUN ls -la && pwd

#COPY ./titanic.csv /tmp/titanic.csv
COPY apiexercise /usr/local/bin/
RUN ls -la /usr/local/bin/

ENTRYPOINT ["apiexercise"]