package main

import (
	"context"
	"errors"
	"fmt"
	. "github.com/API-Exercise/Models"
	"github.com/API-Exercise/dao"
	"github.com/ant0ine/go-json-rest/rest"
	uuid "github.com/nu7hatch/gouuid"
	"log"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
	"net/http"
	"sync"
)
type Service interface {
	Health(w rest.ResponseWriter, r *rest.Request)
	PeopleList(w rest.ResponseWriter, r *rest.Request)
	PersonGet(w rest.ResponseWriter, r *rest.Request)
	PeopleAdd(w rest.ResponseWriter, r *rest.Request)
	PersonUpdate(w rest.ResponseWriter, r *rest.Request)
	PersonDelete(w rest.ResponseWriter, r *rest.Request)
	//FillUp(w rest.ResponseWriter, r *rest.Request)
}
var lock = sync.RWMutex{}
type MicroService struct {
	Env    *Environment
	Dao    dao.MongoDao
	Lock   *sync.RWMutex
}
func MakeMicroService(env *Environment) Service {
	mongoDao := dao.MakeMongoDao(
		env.DATABASENAME,
		env.MONGO_URI,
		env.MONGO_USERNAME,
		env.MONGO_PASSWORD,
		env.LOCAL_DATABASE,
	)
	lock := &sync.RWMutex{}
	return &MicroService{
		Env:    env,
		Dao:    *mongoDao,
		Lock:   lock,
	}
}

func (s *MicroService)Health(w rest.ResponseWriter, r *rest.Request){
	health := s.Dao.Ping()
	log.Printf("===========> %+v\n", health)
	w.WriteJson(health)
}
func (s *MicroService)PeopleList(w rest.ResponseWriter, r *rest.Request) {
	collection := s.Dao.GetCollection(s.Env.COLLECTION_NAME)
	opts := &options.FindOptions{}
	opts.Sort = bson.D{{"type", 1}}
	ctx, cancel := context.WithTimeout(context.Background(), 25*time.Second)
	defer cancel()
	cursor, err := collection.Find(ctx, bson.D{}, opts)

	if err != nil{
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer cursor.Close(ctx)
	people := []*Person{}
	var result bson.M
	for cursor.Next(ctx) {
		person := &Person{}
		err := cursor.Decode(&result)
		if err != nil {
			rest.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		s.Lock.RLock()
		delete(result, "_id")
		bsonBytes, _ := bson.Marshal(result)
		bson.Unmarshal(bsonBytes, &person)
		people = append(people, person)
		s.Lock.RUnlock()
	}
	if err != nil{
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteJson(&people)
}
func (s *MicroService)PersonGet(w rest.ResponseWriter, r *rest.Request) {
	uid := r.PathParam("uuid")
	collection := s.Dao.GetCollection(s.Env.COLLECTION_NAME)
	filter := bson.M{"uuid": uid}
	var result bson.M
	s.Lock.RLock()
	err := collection.FindOne(context.Background(), filter).Decode(&result)
	if err != nil{
		rest.Error(w, "Person not found", http.StatusNotFound)
		return
	}
	person := &Person{}
	bsonBytes, err := bson.Marshal(result)
	if err != nil{
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	bson.Unmarshal(bsonBytes, &person)
	s.Lock.RUnlock()
	w.WriteJson(person)
}
func (s *MicroService)getPerson(filter map[string]interface{}) (p *Person, err error){
	collection := s.Dao.GetCollection(s.Env.COLLECTION_NAME)
	var result bson.M
	err = collection.FindOne(context.Background(), filter).Decode(&result)
	if err != nil{
		return nil, errors.New(fmt.Sprintf("Person not found, error: %s", err.Error()))
	}
	bsonBytes, err := bson.Marshal(result)
	bson.Unmarshal(bsonBytes, &p)
	return p, nil
}
func (s *MicroService)PeopleAdd(w rest.ResponseWriter, r *rest.Request) {

	personData := PersonData{}
	err := r.DecodeJsonPayload(&personData)
	if err != nil {
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if len(personData.Name) == 0{
		rest.Error(w, "Person Name required", http.StatusBadRequest)
		return
	}

	u, err := uuid.NewV4()
	person := &Person{
		PersonData:personData,
		UUID: u.String(),
	}
	s.Lock.Lock()
	collection := s.Dao.GetCollection(s.Env.COLLECTION_NAME)
	resultResp, err := collection.InsertOne(context.Background(), person)

	filter := map[string]interface{}{"_id": resultResp.InsertedID}
	personResp, err := s.getPerson(filter)
	s.Lock.Unlock()
	if err != nil {
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteJson(&personResp)
}
func (s *MicroService)PersonUpdate(w rest.ResponseWriter, r *rest.Request) {
	uid := r.PathParam("uuid")
	personData := PersonData{}
	err := r.DecodeJsonPayload(&personData)
	person := &Person{
		PersonData:personData,
		UUID: uid,
	}
	if err != nil {
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	collection := s.Dao.GetCollection(s.Env.COLLECTION_NAME)
	result := bson.M{}
	update := bson.D{}
	filter := bson.M{"uuid": uid}
	update = bson.D{
		{"$set", person},
	}

	s.Lock.Lock()
	fmt.Printf("======filter====> %+v\n", filter)
	fmt.Printf("======update====> %+v\n", update)
	err = collection.FindOneAndUpdate(context.Background(), filter, update).Decode(&result)
	fmt.Printf("======err====> %+v\n", err)
	personResp := &Person{}
	bsonBytes, err := bson.Marshal(result)
	bson.Unmarshal(bsonBytes, &personResp)

	s.Lock.Unlock()
	if err != nil{
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteJson(&personResp)
}
func (s *MicroService)PersonDelete(w rest.ResponseWriter, r *rest.Request) {
	uid := r.PathParam("uuid")

	collection := s.Dao.GetCollection(s.Env.COLLECTION_NAME)
	filter := bson.M{"uuid": uid}
	result := bson.M{}
	fmt.Printf("======filter====> %+v\n", filter)
	s.Lock.Lock()
	err := collection.FindOneAndDelete(context.Background(), filter).Decode(&result)
	fmt.Printf("======err====> %+v\n", err)
	bsonBytes, err := bson.Marshal(result)
	fmt.Printf("======err====> %+v\n", err)
	person := Person{}
	bson.Unmarshal(bsonBytes, &person)
	s.Lock.Unlock()

	if err != nil{
		rest.Error(w, err.Error(), http.StatusNotFound)
		return
	}
	w.WriteHeader(http.StatusNoContent)
}

//func (s *MicroService)FillUp(w rest.ResponseWriter, r *rest.Request){
//	filename := "/tmp/titanic.csv"
//	f, err := os.Open(filename)
//	if err != nil {
//		fmt.Printf("===err====> %+v\n", err)
//		panic(err)
//	}
//	defer f.Close()
//
//	lines, err := csv.NewReader(f).ReadAll()
//	if err != nil {
//		panic(err)
//	}
//
//	// survived,passengerClass,name,sex,age,siblingsOrSpousesAboard,parentsOrChildrenAboard,fare,uuid
//	for _, line := range lines {
//		survived, _ := strconv.ParseBool(line[0])
//		passengerClass, _ := strconv.ParseInt(line[1], 4, 32)
//		age, _ := strconv.ParseInt(line[4], 4, 32)
//		SiblingsOrSpousesAboard, _ := strconv.ParseInt(line[5], 4, 32)
//		parentsOrChildrenAboard, _ := strconv.ParseInt(line[6], 4, 32)
//		fare, _ := strconv.ParseInt(line[7], 4, 32)
//
//		person := &Person{
//			UUID:line[8],
//			PersonData: PersonData{
//				Survived:survived,
//				PassengerClass:int32(passengerClass),
//				Name:line[2],
//				Sex:getSex(line[3]),
//				Age:int32(age),
//				SiblingsOrSpousesAboard:int32(SiblingsOrSpousesAboard),
//				ParentsOrChildrenAboard:int32(parentsOrChildrenAboard),
//				Fare:float32(fare),
//			},
//		}
//		collection := s.Dao.GetCollection(s.Env.COLLECTION_NAME)
//		resultResp, err := collection.InsertOne(context.Background(), person)
//		fmt.Printf("=======> %+v\n", err)
//		fmt.Printf("==============> %+v\n", resultResp)
//	}
//}
//func getSex(s string) sex.Sex{
//	var i sex.Sex
//	switch s {
//	case "male":
//		i = 1
//	case "female":
//		i = 2
//	default:
//		i = 3
//	}
//	return i
//}