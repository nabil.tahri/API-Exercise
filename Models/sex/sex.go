package sex

import (
	"bytes"
	"encoding/json"
)

type Sex int

const (
	male Sex = iota + 1
	female
	other
)

var toSexString = map[Sex]string{
	male:   "male",
	female: "female",
	other:  "other",
}

var toSexID = map[string]Sex{
	"male":   male,
	"female": female,
	"other":  other,
}

func (typ Sex) MarshalJSON() ([]byte, error) {
	buffer := bytes.NewBufferString(`"`)
	buffer.WriteString(toSexString[typ])
	buffer.WriteString(`"`)
	return buffer.Bytes(), nil
}

func (typ *Sex) UnmarshalJSON(b []byte) error {
	var js string
	err := json.Unmarshal(b, &js)
	if err != nil {
		return err
	}
	*typ = toSexID[js]
	return nil
}

func (typ Sex) String() string {
	return toSexString[typ]
}

