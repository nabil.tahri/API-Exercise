package dao

import (
	"bytes"
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
)

const (
	connectTimeout = 5
	socketTimeout  = 10
	queryTimeout   = 30
)

/*MongoDao represents the Dao*/
type MongoDao struct {
	DatabaseName   string
	MongoClient    *mongo.Client
}

/*MakeMongoDao creates a Dao with the given values*/
func MakeMongoDao(databaseName string, uri string, username string, password string, local bool) *MongoDao {
	var client *mongo.Client
	bctx := context.Background()
	connectionURI := "mongodb://"
	params := make(map[string]string)
	if username != "" && password != "" {
		connectionURI = fmt.Sprintf("%s%s%s%s%s", connectionURI, username, ":", password, "@")
	}
	connectionURI = fmt.Sprintf("%s%s%s%s",connectionURI, uri, "/", databaseName)
	if local {
		params["authMechanism"] = "SCRAM-SHA-1"
	}
	first := true
	buffer := bytes.NewBuffer([]byte(connectionURI))
	for k, v := range params {
		if first {
			buffer.Write([]byte("?"))
			first = false
		} else {
			buffer.Write([]byte("&"))
		}
		buffer.Write([]byte(fmt.Sprintf("%s%s%s", k, "=", v)))
	}

	options := options.Client().ApplyURI(buffer.String()).SetConnectTimeout(connectTimeout * time.Second).SetSocketTimeout(socketTimeout * time.Second)
	client, err := mongo.NewClient(options)

	if err != nil {
		log.Fatal(err)
		return nil
	}

	err = client.Connect(bctx)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Connecting to DocumentDB:", buffer.String())
	mongoDao := &MongoDao{
		MongoClient:    client,
		DatabaseName:   databaseName,
	}

	return mongoDao
}

/*GetCollection return the mongo collection*/
func (d *MongoDao) GetCollection(collectionName string) *mongo.Collection {
	return d.MongoClient.Database(d.DatabaseName).Collection(collectionName)
}

/*Ping to mongo instance */
func (d *MongoDao) Ping() error {
	err := d.MongoClient.Ping(context.Background(), nil)
	if err != nil {
		return err
	}
	return nil
}
