package models

import "github.com/API-Exercise/Models/sex"
/*PersonData */
type PersonData struct {
	Sex sex.Sex `json:"sex" validate:"omitempty"`
	SiblingsOrSpousesAboard int32 `json:"siblingsOrSpousesAboard" validate:"omitempty"`
	Survived bool `json:"survived" validate:"omitempty"`
	Age int32 `json:"age" validate:"omitempty"`
	Fare float32 `json:"fare" validate:"omitempty"`
	Name string `json:"name" validate:"omitempty"`
	ParentsOrChildrenAboard int32 `json:"parentsOrChildrenAboard" validate:"omitempty"`
	PassengerClass int32 `json:"passengerClass" validate:"omitempty"`
}

type People struct {
	People *[]Person `json:"people,omitempty" validate:"omitempty"`
}

type Person struct {
	PersonData
	UUID string `json:"uuid,omitempty" validate:"omitempty"`
}

type Environment struct {
	DATABASENAME          string
	COLLECTION_NAME       string
	MONGO_URI             string
	MONGO_USERNAME        string
	MONGO_PASSWORD        string
	LOCAL_DATABASE		  bool
}