#!/bin/sh
GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" -o apiexercise
docker build -t api-exercise:latest .

docker-compose up --build